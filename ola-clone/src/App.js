import React, { useRef } from "react";
import section1img from './assets/images/section1.jpg';
import { MenuOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css'; 
import "./App.css";

function App() {
  const section1Ref = useRef(null);
  const section2Ref = useRef(null);
  const section3Ref = useRef(null);

  // const scrollTo = (ref) => {
  //   window.scroll({
  //     top: ref.current.offsetTop,
  //     behavior: "smooth",
  //   });
  // };

  return (
    <div className="home">
     <div className="section-1" ref={section1Ref} style={{ height: "100vh",display:'flex', margin: 0,backgroundImage: "url(" + section1img + ")",backgroundSize:"cover" }}>
       <div className="section-1-header" style={{height:'50px',alignItems:'center',justifyContent:'center',width:'100%',display:'flex',color:'#fff',backgroundColor:'rgba(0, 0, 0, 0.2)',letterSpacing:'1px'}}>Next purchase window start Nov 1st</div>
      </div>
      <div ref={section2Ref} style={{ height: "100vh", margin: 0 }}>
        Section 2
      </div>
      <div ref={section3Ref} style={{ height: "100vh", margin: 0 }}>
        Section 3
      </div>
    </div>
  );
}

export default App;
