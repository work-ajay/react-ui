import React, { useState } from 'react';
import Logo from '../../images/logo.png'
import { AlignCenterOutlined,ShoppingCartOutlined,SearchOutlined } from '@ant-design/icons';
import SearchBox from '../utile/searchbox';
import './styles.css';
import { Popover, Drawer } from 'antd';

const NavHeader=()=>{
    const ManuItems=["Categories","Teach on Educa","My Learning"]
    const [visible, setVisible] = useState(false);

    const showDrawer = () => {
      setVisible(true);
    };
  
    const onClose = () => {
      setVisible(false);
    };
    const  nameIcon=()=>{
        return(
            <SearchOutlined  className="filedicon" />
        )
    }
    const content = (
        <div>
            <p>Content ajauuuau</p>
            <p>Content</p>
        </div>
      );
     
    return(
        <div className="nav_header">
            <div className="nav_header_continer">
            <div className="nav_logo_continer">
                <img className="logo" src={Logo}/>
                <div className="titel">Educa</div>
            </div>
               
      <div class="wrapper">
          <SearchBox icon={nameIcon} type="text" placeholder="Search for anything" name="name"/>
</div>
            <div className="nav_items">
                {ManuItems.map((item)=>{
                    if(item=="Categories"){
                        return(
                            <Popover content={content} >
                                <div className="items">{item}</div>
                            </Popover>
                        )
                    }
                    return(<div className="items">{item}</div>)
                    
                })}
                   <ShoppingCartOutlined className="items"  style={{color:'#000',fontSize:"20px",marginRight:'10px'}} />

                   <div className="loginbtn">Login</div>
                   <div className="signupbtn">Signup</div>
               </div>
            <div className="nav_mobile_item">
                 <AlignCenterOutlined onClick={showDrawer} style={{color:'#000',fontSize:"20px",marginRight:'10px'}} />            
            </div>
            </div>
            <Drawer  width="250px" title="Educa" placement="right" onClose={onClose} visible={visible}>
              <div style={{width:'100%',display:'flex',alignItems:'center',flexDirection:"column"}}>
                 {ManuItems.map((item)=>{
                     return <div style={{marginTop:'10px'}}>{item}</div>
                 })}

                   <div className="loginbtn" style={{width:'100%',alignItems:'center',justifyContent:'center',display:'flex',marginTop:'15px'}}>Login</div>
                   <div className="signupbtn"  style={{width:'100%',alignItems:'center',justifyContent:'center',display:'flex',marginTop:'10px'}}>Signup</div>
              </div>
      </Drawer>
        </div>
    )
}

export default NavHeader;