import React from 'react';
import Logo from '../../images/logo.png'

import { ArrowRightOutlined,FacebookOutlined,TwitterOutlined,LinkedinOutlined,InstagramOutlined } from '@ant-design/icons';

import './styles.css'
const Fooder=()=>{
    const explor=["Educa Business","Teach on Educa ","Get the app","Careers","Investors","Blogs","Teachers",];
    const catagories=["Terms","Privacy policy","About us","Contact us",]
    const extralink=["Feedback","My Account","Certificates","Help Center","Newletter"]
    const followus=["Facebook","Twiter","Liknedin","Instagram"]
   const followUsIconRender=(name)=>{
       if(name=="Facebook"){
           return <FacebookOutlined style={{color:"#B6B4B4",marginRight:'7px'}} />
       }else if(name=="Twiter"){
        return <TwitterOutlined style={{color:"#B6B4B4",marginRight:'7px'}} />
       }else if(name=="Liknedin"){
        return <LinkedinOutlined  style={{color:"#B6B4B4",marginRight:'7px'}} />
       }else if(name=="Instagram"){
        return <InstagramOutlined  style={{color:"#B6B4B4",marginRight:'7px'}} />
       }
       else{
           return null
       }
   }
    return(
        <div className="fooder_head">
       <div className="fooder">
           <div className="fooder_continer">
               <div className="section_1">
               <div className="explor">
                   <div className="fooder_titel">Explor</div>
                   {explor.map((item)=>{
                       return(<div className="fooder_items"> <ArrowRightOutlined style={{color:"#B6B4B4",marginRight:'7px'}}  /><span className="fooder_item">{item}</span></div>)
                   })}
               </div>
               <div className="catagories">
               <div className="fooder_titel">Others</div>
                   {catagories.map((item)=>{
                       return(<div className="fooder_items"><ArrowRightOutlined style={{color:"#B6B4B4",marginRight:'7px'}}  /><span className="fooder_item">{item}</span></div>)
                   })}
               </div>
               </div>
               <div className="section_2">
               <div className="extralink">
               <div className="fooder_titel">Extralink</div>
                   {extralink.map((item)=>{
                       return(<div className="fooder_items"><ArrowRightOutlined style={{color:"#B6B4B4",marginRight:'7px'}}  /><span className="fooder_item">{item}</span></div>)
                   })}
               </div>
               <div className="followus">
               <div className="fooder_titel">Follow-Us</div>
                   {followus.map((item)=>{
                       return(<div className="fooder_items">{followUsIconRender(item)}<span className="fooder_item">{item}</span></div>)
                   })}
               </div>
               </div>
           </div>
          

       </div>
       <div className="fooder_food">
       <div className="nav_logo_continer">
                <img className="logo" src={Logo}/>
                <div className="titel" style={{color:"#fff"}}>Educa</div>
            </div>
              <div className="fooder_titel">@ 2021 Educa,inc</div>  
           </div>
       </div>
    )
}

export default Fooder;