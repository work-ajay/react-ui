import React from 'react';
import { UserOutlined ,UnlockOutlined } from '@ant-design/icons';
import Input from '../utile/input';
import './styles.css';
const Login=(props)=>{

   const  nameIcon=()=>{
        return(
            <UserOutlined className="filedicon" />
        )
    }
    const  passIcon=()=>{
        return(
            <UnlockOutlined className="filedicon" />
        )
    }
    return(
        <div className="logincard">
            <div className="logincontiner">
                <div className="login_header">
                    Log In to Your Educa Account!
                </div>
                <div className="loginicon">
                <UserOutlined style={{color:'#000',fontSize:"40px",marginBottom:'20px'}} />  
                    Welcome back, Ajay.               
                 </div>
                 <div className="loginbody">
                   <Input icon={nameIcon} type="text" placeholder="Username" name="name" />
                   <Input icon={passIcon} type="password" placeholder="Password" name="pass" />
                    <div className="logincardbtn">
                         Login
                     </div>
                    
                 </div>
                 <div className="loginfooder">
                  <div className="loginfooder_item_1">or Forgot password</div>
                  <div className="loginfooder_item_2">Log in to a different account</div>
                  <div className="loginfooder_item_3">Don't have an account?<span>Sign up</span></div>
                  <div className="loginfooder_item_4">Log in with your organization</div>
                 </div>
            </div> 

        </div>

    )
}

export default Login;