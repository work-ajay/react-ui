import React from 'react';
import { UserOutlined ,UnlockOutlined,MailOutlined,FacebookOutlined,GoogleOutlined,AppleOutlined,LinkedinOutlined } from '@ant-design/icons';
import Input from '../utile/input';
import  Space  from '../space';
import './styles.css';
const Login=(props)=>{

   const  nameIcon=()=>{
        return(
            <UserOutlined className="filedicon" />
        )
    }
    const  passIcon=()=>{
        return(
            <UnlockOutlined className="filedicon" />
        )
    }
    const  mailIcon=()=>{
        return(
            <MailOutlined className="filedicon" />
        )
    }
    return(
        <div className="logincard">
            <div className="logincontiner">
                <div className="login_header">
                Sign Up and Start Learning!
                </div>
                <Space hig="50px"/>
                {/* <div className="loginicon">
                <UserOutlined style={{color:'#000',fontSize:"40px",marginBottom:'20px'}} />  
                    Welcome back, Ajay.               
                 </div> */}
                 <div className="loginbody">
                   <Input icon={nameIcon} type="text" placeholder="Username" name="name" />
                   <Input icon={mailIcon} type="text" placeholder="Email" name="mail" />
                   <Input icon={passIcon} type="password" placeholder="Password" name="pass" />
                    <div className="logincardbtn">
                         Sign Up
                     </div>
                    
                 </div>
                 <div className="loginfooder">
                 <div className="loginfooder_item_2">or</div>

                     <div className="sociallogins">
                      
                        <FacebookOutlined style={{fontSize:'30px',color:"#3b5998"}} />
                        <GoogleOutlined style={{fontSize:'30px',color:"#1da1f2"}}/>
                        <AppleOutlined style={{fontSize:'30px', color:"#010101"}}/>
                        <LinkedinOutlined style={{fontSize:'30px',color:"#0a66c2"}} />
                     </div>
                 
                  <div className="loginfooder_item_3">Already have an account?<span>Log In</span></div>
                
                 </div>
            </div> 

        </div>

    )
}

export default Login;