import React from 'react';
import './styles.css';
const Input = (props) => {
    return (
        <div className="inputfiled">
            <input type={props.type} className="filed" placeholder={props.placeholder} name={props.name} required />
             {props.icon()}
        </div>
    )
}

export default Input;