import React from 'react';
import './styles.css';
const Input = (props) => {
    return (
        <div className="searchbox">
            <input type={props.type} className="searchboxfiled" placeholder={props.placeholder} name={props.name} required />
             {props.icon()}
        </div>
    )
}

export default Input;