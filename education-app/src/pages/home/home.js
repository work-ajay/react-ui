import React from 'react';
import './styles.css';
import img from '../../images/personaldevelopment.jpg'
 
const Home=()=>{
    const categoriesrow1=[{"name":"Design","img":require('../../images/personaldevelopment.jpg')},{"name":"Development","img":"img"},{"name":"Marketing","img":"img"},{"name":"IT and Software","img":"img"}]
    const categoriesrow2=[{"name":"Personal Development","img":"img"},{"name":"Business","img":"img"},{"name":"Photography","img":"img"},{"name":"Music","img":"img"}]
   const topcategoriesCard=(item)=>{
     return(
        <div className="topcategoriesitems">
        <div className="topcategoriesimg">
            <img src={img} style={{ height:'100%',width:'100%'}}/>
        </div>
        <div className="topcategoriestitel"> {item.name}</div>
    </div>
     )
}
    return(
       <div className="home">
           <div className="homebanner">
              <div className="bannercard">
                  <div className="bannercard_titel">Knowledge opens doors</div>
                  <div className="bannercard_sub">Start exploring new possibilities for your future. Learn from just ₹525 through Dec 14.</div>
              </div> 
           </div>
           <div className="topcategories">
               <div className="topcategoriesheader">Top categories</div>
                <div className="categoriesrow">
                    {categoriesrow1.map((item)=>{
                        return topcategoriesCard(item)
                    })}
                </div>
                <div className="categoriesrow">
                {categoriesrow2.map((item)=>{
                          return topcategoriesCard(item)
                    })}
                </div>
           </div>
           <div className="topcategories">
               <div className="topcategoriesheader">Featured topics by category</div>
                <div className="categoriesrow">
                    {categoriesrow1.map((item)=>{
                        return topcategoriesCard(item)
                    })}
                </div>
              
           </div>
           <div className="Featured topics by category">


           </div>
        </div>
    )
}

export default Home;
