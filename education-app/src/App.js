import './App.css';
import 'antd/dist/antd.css';
import NavHeader from './component/header/navheader';
import Fooder from './component/fooder/fooder';
import Space from './component/space';
import Login from './component/card/login';
import Signup from './component/card/singup';
import Home from './pages/home/home';
const  App=()=> {
  return (
    <div className="App">
       <NavHeader/>
       <Space hig="60px" with="100%"/>
       <Home/>
       <Fooder/>
    </div>
  );
}

export default App;
